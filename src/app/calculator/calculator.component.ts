import { Component, OnInit } from '@angular/core';

import {Equation} from '../equation';
import { Operand } from '../Operand';
import { Sign } from '../Sign';
import { ValueNumber } from '../ValueNumber';
import { Exp } from '../Exp';
import { Token } from '../Token';

import { RandomService } from '../random.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {

  currentEquationBody = '';
  invalidExpression = false;

  one = '1';
  two = '2';
  three = '3';
  four = '4';
  five = '5';
  six = '6';
  seven = '7';
  eight = '8';
  nine = '9';
  zero = '0';
  openB = '(';
  closeB = ')';
  sin = 'sin';
  cos = 'cos';
  tan = 'tan';
  plus = '+';
  minus = '-';
  times = '*';
  div = '/';

  equations: Equation[];

  selectedEquation: Equation;
  onSelect(equation: Equation): void {
    this.selectedEquation = equation;
  }

  /**
   * This function is called when a UI button RAND is pressed. It gets a random number from the random service.
  */
  addRand() {
    this.randomService.getARandomNumber().forEach(i => {
      this.currentEquationBody = this.currentEquationBody + i;
    });
  }

  /**
     * Function that serves as an action point when the user presses the buttons on the calculator. This action
     * is linked to buttons that are used to compose an equation
     * @param txt String containing user input
     */
  addText(txt: string) {
    this.currentEquationBody = this.currentEquationBody + txt;
  }

  /**
   * Function that serves as an action point when the user presses 'CALCULATE'. This action will
   * verify equation composition and update the resulting result.
  */
  calculate() {
    // removing all whitespace and make the string to lowercase
    const temp = new Equation(this.currentEquationBody, this.parse(this.currentEquationBody.toLowerCase().replace(/\s/g, ''), []));
    if (temp.isValid()) {
      this.invalidExpression = false;
      // TODO remove red box (if any)
      // console.log('Valid!');
      temp.evaluate();
      this.equations.push(temp);
      // we need only the last 5
      if (this.equations.length > 5) {
        this.equations = this.equations.splice(1, this.equations.length);
      }
      this.currentEquationBody = '' + temp.result;
    } else {
      // console.log('Incorrect!');
      this.invalidExpression = true;
    }
  }

  /**
     * Function that recursively parses a string and generates a list of Token.
     * In case errors are found, null is returned.
     * @param str String to parse
     * @param tokens List of tokens generated
     */
  parse(str: string, tokens: Token[]) {
    // console.log('Tokens length: ' + tokens.length);
    if (this.isEmpty(str)) {
      return tokens;
    } else {
      switch (str.charAt(0)) {
        case 'c': case 's': case 't': {
          // console.log('in cos/tan/sin');
          // get closing bracket position
          const closeB = str.indexOf(')');
          if (closeB < 5) {
            return null; // error as it might be misplaced or no number detected
          }
          // get number between brackets
          const number = Number(str.substring(4, closeB));
          if (isNaN(number)) {
            return null; // invalid number
          }
          // determine expression type
          switch (str.substring(0, 4)) {
            case 'cos(': {
              tokens.push(new ValueNumber(Math.cos(number)));
              break;
            }
            case 'sin(': {
              tokens.push(new ValueNumber(Math.sin(number)));
              break;
            }
            case 'tan(': {
              tokens.push(new ValueNumber(Math.tan(number)));
              break;
            }
            default: return null;
          }
          // console.log('ValueExp token added');
          return this.parse(str.substring(closeB + 1, str.length), tokens);
        }
        case '+': case '-': case '*': case '/': {
          tokens.push(new Operand(this.getSignforOperand(str.charAt(0))));
          // console.log('Operand token added');
          return this.parse(str.substring(1, str.length), tokens);
        }
        default: { // numbers
          let digit = NaN;
          let i = 0;
          while (i < str.length + 1) {
            // console.log('checking for number at pos 0 and ' + i + ' from string ' + str + ': ' + str.substring(0, i));
            const temp = Number(str.substring(0, i));
            if (!isNaN(temp)) {
              digit = temp;
              i++;
            } else {
              i--;
              break;
            }
          }
          if (isNaN(digit)) {
            return null; // error
          } else {
            tokens.push(new ValueNumber(digit));
            // console.log('ValueNumber token added ' + digit + '. passing ' + str.substring(i, str.length));
            return this.parse(str.substring(i, str.length), tokens);
          }
        }
      }
    }
  }

  /**
   * Function that returns Sign type for string character provided
   * @param str Single character denoting an operator
   */
  getSignforOperand(str: string) {
    switch (str) {
      case '*': return Sign.MULTI;
      case '/': return Sign.DIV;
      case '-': return Sign.MINUS;
      default: return Sign.PLUS;
    }
  }

  /**
   * Function that return TRUE if string is empty
   * @param str String to be evaluated
   */
  isEmpty(str: string) {
    return (!str || 0 === str.length);
  }

  constructor(private randomService: RandomService) {
   }

  ngOnInit() {
    this.equations = [];
  }

}
