import { Token } from './Token';
import { Sign } from './Sign';

export class Operand extends Token {
    sign: Sign;

    toString() {
        return 'Operand with ' + this.sign;
    }

    eval(num1: number, num2: number) {
        console.log('evaluating ... ' + num1 + ' ' + this.sign + ' ' + num2);
        switch (this.sign) {
            case Sign.PLUS: return num1 + num2;
            case Sign.MINUS: return num1 - num2;
            case Sign.MULTI: return num1 * num2;
            case Sign.DIV: return num1 / num2;
            default: return 0;
        }
    }

    constructor(sign: Sign) {
        super();
        this.sign = sign;
    }
}
