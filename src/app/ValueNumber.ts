import { Token } from './Token';

export class ValueNumber extends Token {

    num: number;

    // private _num: number;

    // get num(): number {
    //     return this._num;
    // }

    // set num(theNum) {
    //     this._num = theNum;
    // }

    toString() {
        return 'ValueNumber with number ' + this.num;
    }

    constructor(num: number) {
        super();
        this.num = num;
    }
}
