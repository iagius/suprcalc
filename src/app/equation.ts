import { Token } from './Token';
import { ValueNumber } from './ValueNumber';
import { Operand } from './Operand';

export class Equation {
    body: string;
    result: number;
    tokens: Token[];

    constructor(body: string, tokens: Token[]) {
        this.tokens = tokens;
        this.body = body;
    }

    evaluate() {
        if (this.tokens.length === 0) {
            this.result = 0;
        } else if (this.tokens.length === 1) {
            this.result = (this.tokens[0] as ValueNumber).num;
        } else {
            console.log('evaluating ...');
            let i = 1;
            let first = (this.tokens[0] as ValueNumber).num;
            do {
                console.log('in loop with i ' + i + ' and first ' + first + ' taken from ' + this.tokens[i].toString());
                first = (this.tokens[i] as Operand).eval(first, (this.tokens[i + 1] as ValueNumber).num);
                i += 2;
            } while (i < this.tokens.length);
            console.log('outputting ' + first);
            this.result = first;
        }
    }

    isValid() {
        // console.log('initial token length: ' + this.tokens.length);
        // token list length must be odd to denote start and end with value
        if (this.tokens.length % 2 === 0) {
            return false;
        }
        for (let i = 0; i < this.tokens.length; i++) {
            const token = this.tokens.slice(i, i + 1);
            // console.log('token size reduced to: ' + this.tokens.length);
            // console.log(' --- ' + token.toString() + ' --- ');
            // console.log(token instanceof Operand);
            if ( i % 2 === 0) { // even numbers must be Values
                // console.log('in even number ' + i + ' ' + (token instanceof Operand));
                if (token instanceof Operand) {
                    return false;
                }
            } else { // odd numbers must be Operands
                // console.log('in odd number ' + i + ' ' + !(token instanceof Operand));
                if (token instanceof ValueNumber) {
                    return false;
                }
            }
        }
        return true;
    }

}
