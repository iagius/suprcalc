import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RandomService {

  randomNumberUrl = 'https://www.random.org/integers/?num=1&min=1&max=100&col=1&base=10&format=plain&rnd=new';

  /**
   * This function makes an HTTP call to get a random number and adds it to the input field.
  */
  getARandomNumber(): Observable<number> {
    return this.http.get<number>(this.randomNumberUrl);
  }

  constructor(private http: HttpClient) { }
}
